# Example Archi model for archimate-ci-image

This project provides an example of the [Archi][] model [ArchiMetal][].

The model was published using the [coArchi][] plugin for further visualization
of reports using the CI container [archimate-ci-image][]
([GitLab mirror][archimate-ci-image-mirror]) and [GitHub Action][gh-action].

You can pull image from registries:

* `ghcr.io/woozymasta/archimate-ci-image:5.0.2-1.0.4`
* `docker.io/woozymasta/archimate-ci-image:5.0.2-1.0.4`

Or rootless images:

* `ghcr.io/woozymasta/archimate-ci-image:5.0.2-1.0.4-rootless`
* `docker.io/woozymasta/archimate-ci-image:5.0.2-1.0.4-rootless`

## GitLab CI example

```yml
pages:
  stage: build
  image:
    name: ghcr.io/woozymasta/archimate-ci-image:5.0.2-1.0.4
    entrypoint: [""]

  script:
    - /opt/Archi/entrypoint.sh
    
  variables:
    ARCHI_HTML_REPORT_ENABLED: "true"
    ARCHI_JASPER_REPORT_ENABLED: "true"
    ARCHI_JASPER_REPORT_FORMATS: "PDF,DOCX"
    ARCHI_CSV_REPORT_ENABLED: "false"
    ARCHI_EXPORT_MODEL_ENABLED: "true"

  rules:
    - if: $CI_COMMIT_BRANCH != null || $CI_PIPELINE_SOURCE == "merge_request_event"
      exists:
        - model/folder.xml

  artifacts:
    name: "$CI_JOB_NAME from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 30d
    paths:
      - public
```

## Examples and Demo

[View GitHub Pages demo][demo-gh-pages] | [Example GitHub repository][example-gh]

[View GitLab Pages demo][demo-gl-pages] | [Example GitLab repository][example-gl]

[Archi]: https://www.archimatetool.com
[coArchi]: https://github.com/archimatetool/archi-modelrepository-plugin
[archimate-ci-image]: https://github.com/WoozyMasta/archimate-ci-image
[archimate-ci-image-mirror]: https://gitlab.com/WoozyMasta/archimate-ci-image
[ArchiMetal]: https://github.com/archimatetool/ArchiModels/tree/master/ArchiMetal

[gh-action]: https://github.com/marketplace/actions/deploy-archi-report
[example-gh]: https://github.com/WoozyMasta/archimate-ci-image-example.git
[demo-gh-pages]: https://woozymasta.github.io/archimate-ci-image-example/?view=6213
[example-gl]: https://gitlab.com/WoozyMasta/archimate-ci-image-example
[demo-gl-pages]: https://woozymasta.gitlab.io/archimate-ci-image-example/?view=6213
